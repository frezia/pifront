/*

        Frontend for the pigcd
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import {JsonAPIConfig} from '../utils/JsonAPIConfig';


function defaultCallback(newStatus, id, error) {
};
function defaultStatusCallback(newStatus, err) {
};
function send(gcode, callback) {
  if (callback === undefined) callback = defaultCallback;
  fetch(JsonAPIConfig.exec, {
    headers: { 'Content-Type': 'text/plain; charset=utf-8' },
    method: 'POST',
    body: gcode
  })
    .then((response) => response.json())
    .then(resp => {
      resp.log = {};
      callback(resp, resp.id);
    })
    .catch((err) => callback(undefined, undefined, err));
};
function updateStatus(status, callback) {
  if (callback === undefined) callback = defaultStatusCallback;
  var key;
  for (key = 0; status.log[key] !== undefined; key++) {
  }
  fetch(
    `${JsonAPIConfig.gcodeStatus}/${Math.max(0, key - 1)}`,
    { headers: { 'Content-Type': 'text/plani; charset=utf-8' }, method: 'GET' })
    .then((response) => response.json())
    .then(response => {
      let resp = {...response};
      let result = {};
      if (resp.id !== status.id) {
        result = {...status,...response};
        result.log = {};
      } else {
        result = {...status,...response};
        result.log = {...status.log}
        for (const elem of response.log) {
          result.log[elem.i] = elem;
        }
      }
      callback(result);
    })
    .catch((err) => callback(undefined, err));
};
function getResultLogString(status) {
  var resultString = 'EXECUTION LOG:';
  var holdStatus = (status.hold) ? 'ON HOLD' : '...';
  try {
    for (var key = 0; status.log[key] !== undefined; key++) {
      var value = status.log[key];
      resultString = resultString + '\n[' + key + ']' + value.command + '; ' +
        ((value.log === undefined) ? holdStatus : value.log);
    }
  } catch (e) {
  }
  return resultString;
};


export const GcodeApi = {
  send: send,
  updateStatus: updateStatus,
  getResultLogString: getResultLogString
};




