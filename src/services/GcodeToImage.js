/*

        Frontend for the pigcd
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/



import ImageDrawer from './ImageDrawer';
let {
  getImageDataFromCanvas, 
  putImageDataToCanvas, 
  drawLineOnImageData, 
} = ImageDrawer;
const getGcodeMetrics = (gcode) => {
  let metrics = {
      min:{x:0.0,y:0.0,z:0.0},
      max:{x:0.0,y:0.0,z:0.0},
      indexPositions:[]
  };
  let indexPositions = [];

  let p = { x: 0, y: 0, z: 0};
  let prevkv = {};

  gcode.split("\n").filter(e => ((e.length > 0) && (e.match(/^G\d+/)))).forEach((v) => {
      const arr = v.match(/([GXYZ]\s*[-]{0,1}[\s\d.]+\s*)/g);
      let kv = {};
      arr.forEach(e => { kv[e[0].toLowerCase()] = parseFloat(e.substring(1)); });
      if (kv.g !== undefined) {
          let np = { 
              x: (kv.x !== undefined) ? kv.x : p.x, 
              y: (kv.y !== undefined) ? kv.y : p.y,
              z: (kv.z !== undefined) ? kv.z : p.z
           };
          kv.g = parseInt(kv.g);
          if (kv.g === 0) {
              p = np;
          } else if (kv.g === 1) {
              p = np;
              if (prevkv.g === 0) {
                  indexPositions.push({x:p.x, y:p.y});
              }                 
          } else if (kv.g === 92) {
              p = np;
          }
          prevkv = kv;
      }
      metrics.max.x = Math.max(metrics.max.x,p.x);
      metrics.max.y = Math.max(metrics.max.y,p.y);
      metrics.max.z = Math.max(metrics.max.z,p.z);
      metrics.min.x = Math.min(metrics.min.x,p.x);
      metrics.min.y = Math.min(metrics.min.y,p.y);
      metrics.min.z = Math.min(metrics.min.z,p.z);
  });
  metrics["indexPositions"] = indexPositions;
  return metrics;
};

const toFixedPointPrecision = (num) => {return (Math.round(num * 100) / 100).toFixed(2);};

const drawGcodeOnCanvas = (canvas, gcode, toolDiameter0, canvasPointerXY, scaleFactor_) => {
  
  const scaleFactor = scaleFactor_ ? scaleFactor_ : 1.0;
  let toolDiameter = (toolDiameter0 !== undefined) ? toolDiameter0 : 1.0
  toolDiameter = toolDiameter*scaleFactor;
  if (toolDiameter < 1.0) toolDiameter = 1.0;
  let metrics = getGcodeMetrics(gcode);
  const  width = scaleFactor*(metrics.max.x+20);//
  const  height = scaleFactor*(metrics.max.y+20);//
  canvas.width = width;
  canvas.height = height;
  var context = canvas.getContext('2d');

  let p = { x: 0, y: 0, z: 0 };
  let origin = { x: 0, y: 0, z:0 };

  let toPositionOnCanvas = (p) => {
      return [(p.x + 10 - origin.x)*scaleFactor, height - (p.y + 10 - origin.y)*scaleFactor];
  };
  let fromPositionOnCanvas = (p) => {
      return [p.x/scaleFactor + origin.x -10, -(p.y - height)/scaleFactor + origin.y -10 ];
  };

  {
      let [x,y] = toPositionOnCanvas({x:0, y:0})
      context.moveTo(x,y);
  }

  let imageData = getImageDataFromCanvas(canvas);


  gcode.split("\n").filter(e => ((e.length > 0) && (e.match(/^G\d+/)))).forEach((v) => {
      const arr = v.match(/([GXYZ]\s*[-]{0,1}[\s\d.-]+\s*)/g);
      let kv = {};
      arr.forEach(e => { kv[e[0].toLowerCase()] = parseFloat(e.substring(1)); });
      if (kv.g !== undefined) {
          let np = { 
              x: (kv.x !== undefined) ? kv.x : p.x,
              y: (kv.y !== undefined) ? kv.y : p.y,
              z: (kv.z !== undefined) ? kv.z : p.z  
          };
          kv.g = parseInt(kv.g);
          if (kv.g === 0) {
              p = np;                
          } else if (kv.g === 1) {
              let minz = metrics.min.z;
              let z = Math.min(np.z,p.z);
              if (minz === 0.0) minz = 0.0001;
              let percentage = Math.abs(Math.floor(255.0*Math.abs(z-minz)/minz));
              if (z < 0) {
                  const [x0,y0] = toPositionOnCanvas(p);
                  const [x,y] = toPositionOnCanvas(np);
                  drawLineOnImageData(imageData, x0,y0,x,y,toolDiameter, [percentage,percentage,percentage]);
              }
              p = np;
          } else if (kv.g === 92) {
              p = np;
              origin = np;
              console.error("You should not use G92 in the gcode. It will not show correctly on visualization");
          }
      }
  });
  for (let x = Math.round((metrics.min.x)/10)*10.0; x <= metrics.max.x+5; x += 10.0) {
      const [x0,y0] = toPositionOnCanvas({x:x,y:metrics.min.y-5,z:0});
      const [x1,y1] = toPositionOnCanvas({x:x,y:metrics.max.y+5,z:0});
      if (Math.abs(x) < 0.001) drawLineOnImageData(imageData, x0,y0,x1,y1,1, [255,120,120]);
      else drawLineOnImageData(imageData, x0,y0,x1,y1,1, [120,255,120]);
  }
  for (let y = Math.round((metrics.min.y)/10)*10.0; y <= metrics.max.y+5; y += 10.0) {
      const [x0,y0] = toPositionOnCanvas({x:metrics.min.x-5,y:y,z:0});
      const [x1,y1] = toPositionOnCanvas({x:metrics.max.x+5,y:y,z:0});
      if (Math.abs(y) < 0.001) drawLineOnImageData(imageData, x0,y0,x1,y1,1, [255,120,120]);
      else drawLineOnImageData(imageData, x0,y0,x1,y1,1, [120,255,120]);
  }
  putImageDataToCanvas(canvas, imageData);
  context.beginPath();
  context.font = "12px serif";
  context.fillStyle = "#aa1111ff";
  context.strokeStyle = "#ff1111ff";
  let takenPositions = {};
  let indexPositions = metrics.indexPositions;
  for (let i = 0; i < indexPositions.length; i++) {
      let [x,y] = toPositionOnCanvas({x:indexPositions[i].x, y:indexPositions[i].y,z:0.0});
      let posText = "" + Math.floor(x/10) + "_" + Math.floor(y/20);
      if (takenPositions[posText] === undefined) {
          context.fillText(`${i}`, x, y);
          takenPositions[posText] = 1;
      } else {
          indexPositions[i].y -= 10/(scaleFactor/2);
          indexPositions[i].x += 2/(scaleFactor/2);
          i--;
      }
  }
  if (canvasPointerXY) {
    let coords = fromPositionOnCanvas(canvasPointerXY);
    context.fillText(`${toFixedPointPrecision(coords[0])}mm ${toFixedPointPrecision(coords[1])}mm`, 1,12);
  }
  context.stroke();
};


export default drawGcodeOnCanvas;




