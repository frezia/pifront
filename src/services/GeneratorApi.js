/*

        Frontend for the pigcd
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { JsonAPIConfig } from '../utils/JsonAPIConfig';

function send(b64image, name, settings, callback) {
  if (callback === undefined) {
    console.error("You must provide callback for the generated gcode.");
  };
  fetch(JsonAPIConfig.togcode, {
    headers: { 'Content-Type': 'application/json; charset=utf-8' },
    method: 'POST',
    body: JSON.stringify({ image: b64image, name: name, settings: settings })
  })
    .then((response) => response.json())
    .then((response) => {
      const id = response.id;
      // Function to fetch the result by ID
      const fetchResult = () => {
        fetch(`/api/future/${id}`)
          .then((resultResponse) => resultResponse.json())
          .then((result) => {
            if (result.status === 'ready') {
              console.log(result.value);
              callback(result.value.gcode, null); // Pass the actual result value to the callback
            } else if (result.status === 'deferred' || result.status === 'timeout') {
              setTimeout(fetchResult, 1000); // Retry after 1 second if deferred or timed out
            } else if (result.status === 'not_found') {
              callback(null, 'Result not found.'); // Handle not found error
            } else {
              callback(null, 'Unknown error.'); // Handle unknown error status
            }
          })
          .catch((err) => callback(null, err));
      };

      fetchResult(); // Call the fetchResult function to start fetching the result
    })
    .catch((err) => callback(undefined, err));
};

export const GeneratorApi = {
  send: send
};




