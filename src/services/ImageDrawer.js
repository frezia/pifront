/*

        Frontend for the pigcd
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


// Function to get image data from the canvas
function getImageDataFromCanvas(canvas) {
  const context = canvas.getContext('2d');
  return context.getImageData(0, 0, canvas.width, canvas.height);
}

// Function to put image data back to the canvas
function putImageDataToCanvas(canvas, imageData) {
  const context = canvas.getContext('2d');
  context.putImageData(imageData, 0, 0);
}

// Helper function to set pixel color
function setPixel(imageData, x, y, r, g, b) {
  const index = (y * imageData.width + x) * 4;
  imageData.data[index    ] = r;
  imageData.data[index + 1] = g;
  imageData.data[index + 2] = b;
  imageData.data[index + 3] = 255;
}

// Helper function to set pixel color
function getPixel(imageData, x, y) {
  const index = (y * imageData.width + x) * 4;
  let a = imageData.data[index + 3]
  if (a === 0) {
      return [255,255,255];
  }
  let r = imageData.data[index];
  let g = imageData.data[index + 1];
  let b = imageData.data[index + 2];
  return [r,g,b];
}

// Function to draw a line using Bresenham's algorithm
function drawLineOnImageData(imageData, x1, y1, x2, y2, width, color) {
  x1 = Math.round(x1);
  x2 = Math.round(x2);
  y1 = Math.round(y1);
  y2 = Math.round(y2);
  const [ r, g, b ] = color;
  
  const dx = Math.abs(x2 - x1);
  const dy = Math.abs(y2 - y1);
  const sx = (x1 < x2) ? 1 : -1;
  const sy = (y1 < y2) ? 1 : -1;
  let err = dx - dy;
  
  while (true) {
      // Draw a thick line by setting pixels around the current point
      drawThickPixel(imageData, x1, y1, width, r, g, b);

      if (x1 === x2 && y1 === y2) break;
      const e2 = err * 2;
      if (e2 > -dy) {
          err -= dy;
          x1 += sx;
      }
      if (e2 < dx) {
          err += dx;
          y1 += sy;
      }
  }
}

const setPixelMin = (imageData, x, y, r,g,b) => {
  let px = Math.round(x);
  let py = Math.round(y);
  if (px >= 0 && px < imageData.width && py >= 0 && py < imageData.height) {
      let [nr, ng, nb] = getPixel(imageData, px, py);
      let er = Math.min(r,nr);
      let eg = Math.min(g,ng);
      let eb = Math.min(b,nb);
      setPixel(imageData, px, py, er, eg, eb);
  }
};

const setPixelMax = (imageData, x, y, r,g,b) => {
  let px = Math.round(x);
  let py = Math.round(y);
  if (px >= 0 && px < imageData.width && py >= 0 && py < imageData.height) {
      let [nr, ng, nb] = getPixel(imageData, px, py);
      let er = Math.max(r,nr);
      let eg = Math.max(g,ng);
      let eb = Math.max(b,nb);
      setPixel(imageData, px, py, er, eg, eb);
  }
};

// Helper function to draw a thick pixel (for line width)
function drawThickPixel(imageData, x, y, width, r, g, b, placingMethod) {
  const placeIt = placingMethod ? placingMethod:setPixelMin;
  const halfWidth = Math.max(width / 2,0.0);


  placeIt(imageData, x,y,r,g,b);
  for (let i = -halfWidth; i <= halfWidth; i+=1.0) {
      for (let j = -halfWidth; j <= halfWidth; j+=1.0) {
          const px = x + i;
          const py = y + j;
          if ((1.0*j*j+1.0*i*i) <= (1.0*halfWidth*halfWidth)) {
              placeIt(imageData, px,py,r,g,b);
          }
      }
  } 
};

let ImageDrawer = {
  getImageDataFromCanvas:getImageDataFromCanvas,
  putImageDataToCanvas:putImageDataToCanvas,
  setPixel:setPixel,
  getPixel:getPixel,
  drawLineOnImageData:drawLineOnImageData,
  drawThickPixel:drawThickPixel,
  setPixelMax:setPixelMax,
  setPixelMin:setPixelMin
};


export default ImageDrawer;
