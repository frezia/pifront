import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
//import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
//import 'react-tabs/style/react-tabs.css'; // Import default styles
//import './index.css';
import GcodeExecutor from './components/GcodeExecutor';
import STLToolView from './STLToolView';
import reportWebVitals from './reportWebVitals';
import ImageContainer from './components/ImageContainer';
import ImageToGcodeConverter from './components/ImageToGcodeConverter';
import ImageLoadingFromFile from './components/ImageLoadingFromFile';
import { Card, Container } from '@mui/material';

import PropTypes from 'prop-types';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';


//import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';



function CustomTabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

CustomTabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}


const Root = () => {
  const [activeTab, setActiveTab] = useState(0);
  const [generatedImage, setGeneratedImage] = useState(null);
  const [stlFile, setStlFile] = useState(null);
  const [gcode, setGcode ] = useState('');
  const [isGenerating, setIsGenerating] = useState(false);
  
  const [settings, setSettingsOrig] = useState({
    dpi: 75,
    drill_depth: 3,
    g1drillfeedrate: 0.7,
    g1feedrate: 2,
    multipass: 1,
    safe_cut: 50,
    tool_d: 1.5,
    tool_d_areas: 0,
    dp_epsilon: 0.12,
    fly_z:2,
  });

  const setSettings = (newSettings) => {
    localStorage.setItem('gcodeGeneratorSettings',JSON.stringify(newSettings));
    return setSettingsOrig(newSettings);
  };

  useEffect(() => {
    const items = JSON.parse(localStorage.getItem('gcodeGeneratorSettings'));
    if (items) {
      setSettingsOrig(items);
    }
  }, []);

  const handleChange = (event, newValue) => {
    setActiveTab(newValue);
  };
  return (
    <React.StrictMode>
      <BrowserRouter>


{/*      <Box sx={{ width: '100%' }}> */}
      <Container>
      <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
        {/*<Tabs selectedIndex={activeTab} onSelect={handleSelectTab}> */}
        <Tabs value={activeTab} onChange={handleChange} aria-label="basic tabs example">
            <Tab label="Input PNG" {...a11yProps(0)} />
            <Tab label="Input STL" {...a11yProps(1)} />
        </Tabs>
        </Box>
            {activeTab === 0 && <ImageLoadingFromFile 
            setImage={setGeneratedImage} />}

            {activeTab === 1 && <STLToolView 
            generatedImage={generatedImage} setGeneratedImage={setGeneratedImage} 
            stlFile={stlFile} setStlFile={setStlFile}
            dpi={settings.dpi} setDpi={dpi => setSettings({...settings, dpi})}
            />}
    </Container>

{/*      </Box> */ }



        {generatedImage && <ImageContainer imageDataUrl={generatedImage} />}


        <ImageToGcodeConverter 
            image={generatedImage}
            gcodeReadyHandler={setGcode}
            onSettings={(name,value) => setSettings({ ...settings, [name]: value }) }
            settings={settings}
            isGenerating={isGenerating}
            setIsGenerating={setIsGenerating}
            />


<GcodeExecutor gcode={gcode}
          setGcode={setGcode}
          settings={settings}
          isGenerating={isGenerating}
        />
        <Container>
        <h2>Quick pick examples</h2>
        <pre>
          ; go to 0,0,0 on classical machine
          G28
          G92X0Y0Z0
          G0ZZ-33.6069
          G92X0Y0Z0
        </pre>
        <pre>
          ; go to 0,0,0 starting at Z=0
          G28
          G0Z0
          G92X0Y0Z0
        </pre>
        </Container>
      </BrowserRouter>
    </React.StrictMode>
  );
};

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<Root />);

reportWebVitals();
