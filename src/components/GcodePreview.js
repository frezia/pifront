
/*

        Frontend for the pigcd
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// import { shape } from '@mui/system';
import React, { useEffect, useRef, useState } from 'react';

import drawGcodeOnCanvas from '../services/GcodeToImage';


const GcodePreview = ({gcode, toolDiameter}) => {
    // const [dragX0, setDragX0] = useState(0);
    // const [dragY0, setDragY0] = useState(0);
    // const [dXY, setDXY] = useState({x:0,y:0});
    const [scaleFactor, setScaleFactor ] = useState(1.0)
    // const [canvasShiftXY, setCanvasShiftXY ] = useState()
    const [canvasPointerXY, setCanvasPointerXY ] = useState({x:-1, y:-1})
    const [canvasDebouncedValues, setCanvasDebouncedValues]=useState(false);
    const canvasRef = useRef(null);


  useEffect(() => {
    const timer = setTimeout(() => {
        setCanvasDebouncedValues({
            gcode:gcode, 
            toolDiameter:toolDiameter, 
            canvasPointerXY:canvasPointerXY,
            scaleFactor:scaleFactor});
    }, 250);
    return () => clearTimeout(timer);
  }, [gcode, toolDiameter, canvasPointerXY,scaleFactor]);

  useEffect(() => {
    if (canvasDebouncedValues) {
        const canvas = canvasRef.current;
        if (canvas) {
          drawGcodeOnCanvas(canvas, canvasDebouncedValues.gcode, canvasDebouncedValues.toolDiameter, canvasDebouncedValues.canvasPointerXY, canvasDebouncedValues.scaleFactor);
        }
      }
  }, [canvasDebouncedValues]);


    /*const onDraggingEvents = (evt) => {
      let rect = evt.target.getBoundingClientRect();
      let x = evt.clientX - rect.left;
      let y = evt.clientY - rect.top;
      if (evt.type === "mousedown") {
        setDragX0(x);
        setDragY0(y);
      } else if (evt.type === "mouseup") {
        //console.log(x - dragX0, y - dragY0);
      }
    }; */

    const onMouseMoveEvents = (evt) => {
        let rect = evt.target.getBoundingClientRect();
        let x = evt.clientX - rect.left;
        let y = evt.clientY - rect.top;
        setCanvasPointerXY({x:x, y:y});
    }; 
  
    const onScaleValueChange = (evt) => {
        setScaleFactor(evt.target.value);
    };

    return (
        <div>
        <div>
            <input type='range' min={0.5} max={10} step={0.5} value={scaleFactor} onChange={onScaleValueChange}/>
        </div>
        <div>
        <canvas
          ref={canvasRef}
          // onMouseDown={onDraggingEvents}
          // onMouseUp={onDraggingEvents}
          onMouseMove={onMouseMoveEvents}
        />
        </div>
      </div>
    );
  };
  
  export default GcodePreview;