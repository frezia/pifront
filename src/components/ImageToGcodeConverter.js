/*

        Frontend for the pigcd
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import React, { useState, useEffect, useRef } from 'react';
import { GeneratorApi } from '../services/GeneratorApi';
//import ImageContainer from './ImageContainer';

import CanvasImageContainer from './CanvasImageContainer';
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Container from "@mui/material/Container";
import { Box, FormGroup } from '@mui/material';

const ImageToGcodeConverter = ({settings, onSettings, gcodeReadyHandler, image, isGenerating, setIsGenerating}) => { //(props) => {
  

  const generateGcode = () => {
    if (!image) return;
    setIsGenerating(true);
    GeneratorApi.send(
      image,
      "mergedIimage",
      {
        dpi: settings.dpi,
        drill_depth: settings.drill_depth,
        g1drillfeedrate: settings.g1drillfeedrate,
        g1feedrate: settings.g1feedrate,
        multipass: settings.multipass,
        safe_cut: settings.safe_cut,
        tool_d: settings.tool_d,
        dp_epsilon: settings.dp_epsilon,
        tool_d_areas: settings.tool_d_areas,
        fly_z: settings.fly_z
        
      },
      (ret, err) => {
        if (ret) {
          gcodeReadyHandler(ret);
        }
        if (err) {
          console.log("Error generating gcode: " + err);
          alert("Error generating gcode");
        }
        setIsGenerating(false);
      }
    );
  };

  const onSettingsChange = (event) => {
    onSettings(event.target.name, parseFloat(event.target.value));
  };

  let tool_d_p = {inputProps:{ min:"0", max:"5", inputMode: 'numeric', pattern: '[0-9.]*'     },helperText:"Diameter of the tool [mm]"};

  return (
    <Container className='ImageToGcodeConverter'>
      <Box
      component="form"
      sx={{
        '& .MuiTextField-root': { m: 1, width: '25ch' },
      }}
      noValidate
      autoComplete="off"
    >

      <TextField
          key={"tool_d"}
          onChange={onSettingsChange}
          inputProps={{
            min: tool_d_p.inputProps.min,
            max: tool_d_p.inputProps.max,
            inputMode: tool_d_p.inputProps.inputMode,
            pattern: tool_d_p.inputProps.pattern
          }}
          name={"tool_d"}
          label={"tool_d"}
          defaultValue={settings["tool_d"]}
          helperText={tool_d_p.elperText}
        />
      {(!(isGenerating || !image)) && Object.entries({
        drill_depth: {inputProps:{ min:"0", max:"30", inputMode: 'numeric', pattern: '[0-9.]*'    },helperText:"How deep do we need to cut the material [mm]"},
//         tool_d: {inputProps:{ min:"0", max:"5", inputMode: 'numeric', pattern: '[0-9.]*'     },helperText:"Diameter of the tool [mm]"},
        g1feedrate: {inputProps:{ min:"0.5", max:"120", inputMode: 'numeric', pattern: '[0-9.]*' },helperText:"Speed of cutting [mm/s]"},
        g1drillfeedrate: {inputProps:{ min:"10", max:"120", inputMode: 'numeric', pattern: '[0-9.]*'  },helperText:"Speed of drill movements [mm/s]"},
        dpi: {inputProps:{ min:"20", max:"1200", inputMode: 'numeric', pattern: '[0-9.]*' },helperText:"Resolution of the source image [dpi]."},
        multipass: {inputProps:{ min:"1", max:"100", inputMode: 'numeric', pattern: '[0-9]*'    },helperText:"How many cutting iterations"},
        safe_cut: {inputProps:{ min:"0", max:"255", inputMode: 'numeric', pattern: '[0-9]*'    },helperText:"The amount of left material to keep the elements attached to the rest of material (0-255)"},
        tool_d_areas: {inputProps:{ min:"0", max:"5", inputMode: 'numeric', pattern: '[0-9.]*'     },helperText:"The thickness of areas cut or 0 if no areas [mm]"},
        dp_epsilon: {inputProps:{ min:"0.01", max:"3", inputMode: 'numeric', pattern: '[0-9.]*'     },helperText:"Ramer–Douglas–Peucker epsilon value [mm]"},
        fly_z: {inputProps:{ min:"0.1", max:"20.0", inputMode: 'numeric', pattern: '[0-9]*[0-9.][0-9]*'     },helperText:"Distance to fly above material during idle moves [mm]"},
      }).map(([name, {inputProps,helperText}]) => (
        <TextField
          key={name}
          onChange={onSettingsChange}
          inputProps={{
            min: inputProps.min,
            max: inputProps.max,
            inputMode: inputProps.inputMode,
            pattern: inputProps.pattern
          }}
          name={name}
          label={name}
          defaultValue={settings[name]}
          helperText={helperText}
        />
      ))}
      {(!(isGenerating || !image)) && <pre>Please load some image</pre>}
      <Button variant="outlined" onClick={generateGcode} disabled={isGenerating || !image}>
        Generate g-code
      </Button>
      </Box>

    </Container>
  );
};

export default ImageToGcodeConverter;
//  <TextField onChange={this.onSettingsChange}                                                                                  name="file" label="file" defaultValue={""} helperText="The project name." />
