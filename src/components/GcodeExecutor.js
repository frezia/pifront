/*

        Frontend for the pigcd
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import React, { useState, useEffect, useRef } from 'react'
import Button from '@mui/material/Button'
import TextareaAutosize from '@mui/material/TextareaAutosize'
import Switch from '@mui/material/Switch'
import GcodePreview from './GcodePreview.js'
import Container from '@mui/material/Container'
import FormGroup from '@mui/material/FormGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import SendIcon from '@mui/icons-material/Send';
import CancelIcon from '@mui/icons-material/Cancel';

import { GcodeApi } from '../services/GcodeApi.js'


const GcodeExecutor = ({ settings, gcode, setGcode, isGenerating}) => {
  // const [isExecuting, setExecuting] = useState(false)
  // const [isHold, setHold] = useState(false)
  const [lastApiResponse, setLastApiResponse] = useState({log:{}, id:-1})
  const [showGcodePreview, setShowGcodePreview] = useState(false)
  const [showGcodeSource, setShowGcodeSource] = useState(false)
  const [showGcodeStatus, setShowGcodeStatus] = useState(false)

  const handleApiResponseToUpdateStatus = (statusUpdate, id, err) => {
    //console.log('handleApiResponseToUpdateStatusA', lastApiResponse, statusUpdate);
    var newRespMerged = {...lastApiResponse,...statusUpdate};
    //console.log('handleApiResponseToUpdateStatusB', newRespMerged);
    // console.log(lastApiResponse,statusUpdate, newRespMerged);
 // {"exec_index":0,"executing":true,"hold":false,"id":35,"note":"Running in parallel. You can check the status by ","status":"ok"}
    setLastApiResponse(newRespMerged);
    // exec_index
    // executing
    // hold
    // id
    // log
    // note
    // status
  }

  const sendStatusRequest = () => {
    GcodeApi.send('?', handleApiResponseToUpdateStatus)
  }
  const sendGcodeRequest = () => {
    console.log('sending gcode', gcode)
    GcodeApi.send(gcode, handleApiResponseToUpdateStatus)
  }
  const sendHoldRequest = () => {
    GcodeApi.send('!', handleApiResponseToUpdateStatus)
  }
  const sendResumeRequest = () => {
    GcodeApi.send('~', handleApiResponseToUpdateStatus)
  }
  const sendCancelRequest = () => {
    GcodeApi.send('&', handleApiResponseToUpdateStatus)
  }

  // Use useRef to store the interval ID so that it can be cleared when the component unmounts
  const intervalIdRef = useRef(null)

  // useEffect hook to start the interval when the component mounts
  useEffect(() => {
    // Define the function to be called at each interval
    const intervalFunction = () => {
        GcodeApi.updateStatus(lastApiResponse, (statusUpdate, err) => {
                if (err) {
                    //statusUpdate['logText'] = [err];
                    console.error(err)
                } else {
                    statusUpdate['logText'] = GcodeApi.getResultLogString(statusUpdate);
                    setLastApiResponse(statusUpdate); /// TODO
                }
            });    
    }

    // Start the interval and store the interval ID in the ref
    intervalIdRef.current = setInterval(intervalFunction, 3000)

    // Clean up the interval when the component unmounts
    return () => {
      clearInterval(intervalIdRef.current)
    }
  }, [lastApiResponse]) // Empty dependency array ensures the effect runs only once when the component mounts

  return (
    <Container className='GcodeExecutor'>
      <Button variant="contained" disabled={lastApiResponse.executing || isGenerating} onClick={sendGcodeRequest} endIcon={<SendIcon />}>
        Execute
      </Button>
      <Button variant="outlined" onClick={sendStatusRequest}>Status</Button>

      <Button
        variant="outlined"
        disabled={!(!lastApiResponse.hold && lastApiResponse.executing)}
        onClick={sendHoldRequest}
      >
        Hold
      </Button>
      <Button variant="outlined" 
        disabled={!(lastApiResponse.hold && lastApiResponse.executing)}
        onClick={sendResumeRequest}
      >
        Resume
      </Button>
      <Button variant="outlined"  disabled={!lastApiResponse.executing} onClick={sendCancelRequest} endIcon={<CancelIcon />}>
        Terminate
      </Button>



<FormGroup>
<FormControlLabel control={<Switch checked={showGcodePreview} onChange={(event, checked) => setShowGcodePreview(checked)} />} label="Visual preview" />
<FormControlLabel control={<Switch checked={showGcodeSource} onChange={(event, checked) => setShowGcodeSource(checked)} />} label="Program source" />
<FormControlLabel control={<Switch checked={showGcodeStatus} onChange={(event, checked) => setShowGcodeStatus(checked)} />} label="Execution status" />
</FormGroup>
    {(showGcodePreview || showGcodeSource || showGcodeStatus) && <h2>Program details</h2>}
    { showGcodePreview && <GcodePreview gcode={gcode} toolDiameter={settings.tool_d} /> }
    { showGcodeSource && <TextareaAutosize value={gcode} onChange={e => setGcode(e.target.value)}></TextareaAutosize>}
    { showGcodeStatus && <TextareaAutosize value={lastApiResponse.logText} readOnly></TextareaAutosize>}
    {lastApiResponse.status && <span >{lastApiResponse.status}</span>}
    </Container>
  )
}
export default GcodeExecutor

/*



        */
