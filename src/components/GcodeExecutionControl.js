/*

        Frontend for the pigcd
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import React, { useState, useEffect, useCallback } from 'react';
import { GcodeApi } from '../services/GcodeApi.js';
import Button from "@mui/material/Button";
import TextareaAutosize from "@mui/material/TextareaAutosize";
import Switch from "@mui/material/Switch";
import { GcodePreview } from './GcodePreview.js';

export function GcodeExecutionControl({ gcode, toolDiameter, gcodeReadyHandler }) {
    const [log, setLog] = useState({});
    const [logText, setLogText] = useState("");
    const [status, setStatus] = useState("");
    const [executing, setExecuting] = useState(false);
    const [hold, setHold] = useState(false);
    const [showGcodeSource, setShowGcodeSource] = useState(false);
    const [showGcodeStatus, setShowGcodeStatus] = useState(false);
    const [showGcodePreview, setShowGcodePreview] = useState(false);

    const onGcodeTextValueChange = useCallback((e) => {
        gcodeReadyHandler(e.target.value);
    }, [gcodeReadyHandler]);

    const tick = useCallback(() => {
        GcodeApi.updateStatus({ log, logText, status, executing, hold }, (statusUpdate, err) => {
            if (err) {
                setLog(err);
            } else {
                setLogText(GcodeApi.getResultLogString({ log, logText, status, executing, hold }));
                setStatus(statusUpdate.status);
            }
        });
    }, [log, logText, status, executing, hold]);

    useEffect(() => {
        const timerID = setInterval(() => tick(), 5000);
        return () => clearInterval(timerID);
    }, [tick]);

    const sendRequest = useCallback((command) => {
        GcodeApi.send(command, (statusUpdate) => setStatus(statusUpdate));
    }, []);

    return (
        <div>
            <h2>Gcode controls</h2>
            <Button disabled={executing} onClick={() => sendRequest(gcode)}>Send</Button>
            <Button onClick={() => sendRequest("?")}>Status</Button>
            <Button disabled={!(!hold && executing)} onClick={() => sendRequest("!")}>Hold</Button>
            <Button disabled={!(hold && executing)} onClick={() => sendRequest("~")}>Resume</Button>
            <Button disabled={!executing} onClick={() => sendRequest("&")}>Terminate</Button>

            <span>
                <Switch checked={showGcodePreview} onChange={() => setShowGcodePreview(prev => !prev)} />
                Visual preview
            </span>
            <span>
                <Switch checked={showGcodeSource} onChange={() => setShowGcodeSource(prev => !prev)} />
                Program source
            </span>
            <span>
                <Switch checked={showGcodeStatus} onChange={() => setShowGcodeStatus(prev => !prev)} />
                Execution status
            </span>
            
            {(showGcodePreview || showGcodeSource || showGcodeStatus) && <h2>Program details</h2>}
            {showGcodePreview && <GcodePreview gcode={gcode} toolDiameter={toolDiameter} width="500" height="500" />}
            {showGcodeSource && <TextareaAutosize value={gcode} onChange={onGcodeTextValueChange} />}
            {showGcodeStatus && <TextareaAutosize value={logText} readOnly />}
            {status && <span>{status}</span>}
        </div>
    );
}
