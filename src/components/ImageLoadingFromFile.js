/*

        Frontend for the pigcd
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import React, { useState, useEffect, useRef } from 'react';
import { GeneratorApi } from '../services/GeneratorApi';
//import ImageContainer from './ImageContainer';

import CanvasImageContainer from './CanvasImageContainer';
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Container from "@mui/material/Container";
import { Box, Slider } from '@mui/material';

const ImageLoadingFromFile = ({ setImage}) => { //(props) => {
    const [blackThreshold, setBlackThreshold] = useState(0);
    const [imgFile, setImgFile] = useState();

  const drawImageInsideCanvasToRemoveTransparentBackground = async (f, onImageReady, thresholdSrc) => {
    if (f === undefined) return;
    const threshold = thresholdSrc?thresholdSrc:parseInt( blackThreshold );
    
    const imgSrc = await fileToBase64(f);

    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    const img = new Image();
    img.src = imgSrc;
    img.onload = () => {
        ctx.canvas.width = img.width;
        ctx.canvas.height = img.height;
        ctx.fillStyle = 'black';
        ctx.rect(0, 0, img.width, img.height);
        ctx.fill();
        ctx.drawImage(img, 0, 0, img.width, img.height);

        console.log(threshold);
        if (threshold > 0) {
            const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
            const data = imageData.data;

            for (let i = 0; i < data.length; i += 4) {
            const brightness = (data[i] + data[i + 1] + data[i + 2]) / 3;
            if (brightness < threshold) {
                data[i] = 0;       // Red
                data[i + 1] = 0;   // Green
                data[i + 2] = 0;   // Blue
            }
            }
            ctx.putImageData(imageData, 0, 0);
        }
        onImageReady(canvas.toDataURL());
    };
  };

  const handleFileRead = async (event) => {
    let f = undefined
    for (let file of event.target.files) {
      f = file;
    }
    setImgFile(f);
    drawImageInsideCanvasToRemoveTransparentBackground(f, (nImgSrc)=>{
      setImage(nImgSrc);
    });

  };

  const fileToBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        resolve(reader.result);
      };
      reader.onerror = (error) => {
        reject(error);
      };
    });
  };

  return (
    <Container className='ImageLoadingFromFile'>
    <Box
      component="form"
      sx={{
        '& > :not(style)': { m: 1, width: '25ch' },
      }}
      noValidate
      autoComplete="off"
    >
<Button variant="contained" component="label" >Select images (png)
    <input type="file" accept="image/png, image/gif, image/jpeg" onChange={handleFileRead} hidden />
  </Button>
        <Slider aria-label="rotationX" value={blackThreshold}  disabled={imgFile === undefined}  step={1} min={0} max={254} onChange={event => {
            setBlackThreshold(event.target.value);
            drawImageInsideCanvasToRemoveTransparentBackground(imgFile,setImage, parseInt(event.target.value));
        }
            } />
</Box>
    </Container>
  );
};

export default ImageLoadingFromFile;
//  <TextField onChange={this.onSettingsChange}                                                                                  name="file" label="file" defaultValue={""} helperText="The project name." />
