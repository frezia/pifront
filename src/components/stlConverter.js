/*

        Frontend for the pigcd
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


export const service_stl_converter = () => {

    const rotateAxisZ = (p, q) => {
        const np = {
          x: p.x * Math.cos(q) - p.y * Math.sin(q),
          y: p.x * Math.sin(q) + p.y * Math.cos(q),
          z: p.z
        };
        return np;
      };
      //p: FVertex, q: number
      const rotateAxisY = (p, q) => {
        const np = {
          y: p.y * Math.cos(q) - p.z * Math.sin(q),
          z: p.y * Math.sin(q) + p.z * Math.cos(q),
          x: p.x
        };
        return np;
      };
      // p: FVertex, q: number
      const rotateAxisX = (p, q) => {
        const np = {
          z: p.z * Math.cos(q) - p.x * Math.sin(q),
          x: p.z * Math.sin(q) + p.x * Math.cos(q),
          y: p.y
        };
        return np;
      };
    
    
      // p: FVertex, v: FVertex[]
      const getZValueForPlane = (p, v) => {
        const [x, y, tmpZ] = [p.x, p.y, p.z];
        const e0 =
          (x * (v[0].y * (v[1].z - v[2].z) + v[0].z * (v[2].y - v[1].y) +
            v[1].y * v[2].z - v[1].z * v[2].y));
        const e1 =
          (v[0].x * (v[2].y - v[1].y) + v[0].y * (v[1].x - v[2].x) -
            v[1].x * v[2].y + v[1].y * v[2].x);
        const e2 =
          (y * (-v[0].x * v[1].z + v[0].x * v[2].z + v[0].z * v[1].x -
            v[0].z * v[2].x - v[1].x * v[2].z + v[1].z * v[2].x));
        const e3 =
          (-v[0].x * v[1].y + v[0].x * v[2].y + v[0].y * v[1].x -
            v[0].y * v[2].x - v[1].x * v[2].y + v[1].y * v[2].x);
        const e4 =
          (-v[0].x * v[1].y * v[2].z + v[0].x * v[1].z * v[2].y +
            v[0].y * v[1].x * v[2].z - v[0].y * v[1].z * v[2].x -
            v[0].z * v[1].x * v[2].y + v[0].z * v[1].y * v[2].x);
        const e5 =
          (-v[0].x * v[1].y + v[0].x * v[2].y + v[0].y * v[1].x -
            v[0].y * v[2].x - v[1].x * v[2].y + v[1].y * v[2].x);
        const z = e0 / e1 + e2 / e3 + e4 / e5;
        return z;
      };
    
    
      const putPixel = (data, x, y, c) => {
        const pix = data.data;
        const i = (y * (data.width) + x) * 4;
        pix[i] = c;
        pix[i + 1] = c;
        pix[i + 2] = c;
        pix[i + 3] = 255;  // blue
      };
    
      const getPixel = (data, x, y) => {
        const pix = data.data;
        const i = (y * (data.width) + x) * 4;
        const c = (pix[i] + pix[i + 1] + pix[i + 2]) / 3;
        return c;
      };
    
    
      /*
      http://www.sunshine2k.de/coding/java/TriangleRasterization/TriangleRasterization.html#algo2
      */
      //    img: ImageData, T: FVertex[], color: number[],fmaxmin
      const drawTriangle = (
        img, T, color, fmaxmin
      ) => {
        const f = fmaxmin ? fmaxmin : (a, b) => { return Math.max(a, b); };
        const v1 = T[0];
        const v2 = T[1];
        const v3 = T[2];
        // za http://forum.devmaster.net/t/advanced-rasterization/6145
        // 28.4 fixed-point coordinates
        const Y1 = Math.round(16.0 * v1.y);
        const Y2 = Math.round(16.0 * v2.y);
        const Y3 = Math.round(16.0 * v3.y);
    
        const X1 = Math.round(16.0 * v1.x);
        const X2 = Math.round(16.0 * v2.x);
        const X3 = Math.round(16.0 * v3.x);
    
        // Deltas
        const DX12 = X1 - X2;
        const DX23 = X2 - X3;
        const DX31 = X3 - X1;
    
        const DY12 = Y1 - Y2;
        const DY23 = Y2 - Y3;
        const DY31 = Y3 - Y1;
    
        // Fixed-point deltas
        const FDX12 = DX12 << 4;
        const FDX23 = DX23 << 4;
        const FDX31 = DX31 << 4;
    
        const FDY12 = DY12 << 4;
        const FDY23 = DY23 << 4;
        const FDY31 = DY31 << 4;
    
        // Bounding rectangle
        const minx = (Math.min(X1, Math.min(X2, X3)) + 0xF) >> 4;
        const maxx = (Math.max(X1, Math.max(X2, X3)) + 0xF) >> 4;
        const miny = (Math.min(Y1, Math.min(Y2, Y3)) + 0xF) >> 4;
        const maxy = (Math.max(Y1, Math.max(Y2, Y3)) + 0xF) >> 4;
    
        let currentY = miny;
    
        // Half-edge constants
        let C1 = DY12 * X1 - DX12 * Y1;
        let C2 = DY23 * X2 - DX23 * Y2;
        let C3 = DY31 * X3 - DX31 * Y3;
    
        // Correct for fill convention
        if (DY12 < 0 || (DY12 === 0 && DX12 > 0)) {
          C1++;
        }
        if (DY23 < 0 || (DY23 === 0 && DX23 > 0)) {
          C2++;
        }
        if (DY31 < 0 || (DY31 === 0 && DX31 > 0)) {
          C3++;
        }
    
        let CY1 = C1 + DX12 * (miny << 4) - DY12 * (minx << 4);
        let CY2 = C2 + DX23 * (miny << 4) - DY23 * (minx << 4);
        let CY3 = C3 + DX31 * (miny << 4) - DY31 * (minx << 4);
    
    
    
        const colr = Math.round((color[0] + color[1] + color[2]) / 3.0) % 0x0ff;
        for (let y = miny; y < maxy; y++) {
          let CX1 = CY1;
          let CX2 = CY2;
          let CX3 = CY3;
    
          for (let x = minx; x < maxx; x++) {
            if (CX1 > 0 && CX2 > 0 && CX3 > 0) {
              putPixel(
                img, x, currentY,
                f(getPixel(img, x, currentY),
                  getZValueForPlane({ x: x, y: currentY, z: 1.0 }, T)));
            }
    
            CX1 -= FDY12;
            CX2 -= FDY23;
            CX3 -= FDY31;
          }
    
          CY1 += FDX12;
          CY2 += FDX23;
          CY3 += FDX31;
    
          currentY++;
        }
      };
    
      const resetImage = (canvas, size) => {
        const ctx = canvas.getContext('2d');
        if (size.width) {
          canvas.width = size.width;
        } else {
          canvas.width = 1;
        }
        if (size.height) {
          canvas.height = size.height;
        } else {
          canvas.height = 1;
        }
        ctx.font = '20px Comfortaa';
        ctx.fillText('(empty)', 10, 50);
      };
    
      // p: FVertex, q: number
      // stl3D: Stl3D, dpi: number, rotation: FVertex
      const stl3DDrawSimple = (canvas, stl3D, dpi, rotation, margin_) => {
        const margin = margin_?margin_:2.0;
        const ctx = canvas.getContext('2d');
        const that = this;
        const tmpstl3D = {
          min: { x: 10000, y: 10000, z: 10000 },
          max: { x: -10000, y: -10000, z: -10000 },
          faces: []
        };
    
        stl3D.faces.forEach(function (face, index, arr) {
          const nface = {
            normal: {
              x: face.normal.x,
              y: face.normal.y,
              z: face.normal.z
            },
            vertices: []
          };
          face.vertices.forEach(function (v0, i) {
            const v = rotateAxisX(rotateAxisY(rotateAxisZ(v0, rotation.z), rotation.y), rotation.x);
            if (v.x < tmpstl3D.min.x) {
              tmpstl3D.min.x = v.x;
            }
            if (v.x > tmpstl3D.max.x) {
              tmpstl3D.max.x = v.x;
            }
            if (v.y < tmpstl3D.min.y) {
              tmpstl3D.min.y = v.y;
            }
            if (v.y > tmpstl3D.max.y) {
              tmpstl3D.max.y = v.y;
            }
            if (v.z < tmpstl3D.min.z) {
              tmpstl3D.min.z = v.z;
            }
            if (v.z > tmpstl3D.max.z) {
              tmpstl3D.max.z = v.z;
            }
            nface.vertices.push(v);
          });
          tmpstl3D.faces.push(nface);
        });
        stl3D = tmpstl3D;
        const scaleXY = dpi * 0.03937008;
        const shiftX = scaleXY * (margin - stl3D.min.x);
        const shiftY = scaleXY * (margin + stl3D.max.y);
        canvas.width = scaleXY * (stl3D.max.x + margin*2 - stl3D.min.x);
        canvas.height = scaleXY * (stl3D.max.y + margin*2 - stl3D.min.y);
    
        const scaleZ = 1.0 / (stl3D.max.z - stl3D.min.z);
        const shiftZ = -stl3D.min.z * scaleZ;
        ctx.imageSmoothingEnabled = false;
        ctx.fillStyle = "black";
        ctx.fillRect(0, 0, canvas.width, canvas.height);
    
        const imdata = ctx.getImageData(0, 0, canvas.width, canvas.height);
    
        const fx = (x) => x * scaleXY + shiftX;
        const fy = (y) => (-y) * scaleXY + shiftY;
        const fz = (z) => z * scaleZ + shiftZ;
        for (let i = 0; i < stl3D.faces.length; i++) {
          stl3D.faces[i].normal.x = stl3D.faces[i].normal.y =
            stl3D.faces[i].normal.z = 255.0 *
            (fz(stl3D.faces[i].vertices[0].z) + fz(stl3D.faces[i].vertices[1].z) +
              fz(stl3D.faces[i].vertices[2].z)) /
            3.0;
        }
        for (let i = 0; i < stl3D.faces.length; i++) {
          drawTriangle(
            imdata, stl3D.faces[i].vertices.map((e) => {
              return { x: fx(e.x), y: fy(e.y), z: fz(e.z) * 256.0 };
            }),
            stl3D.faces[i].vertices.map((e) => 255.0 * fz(e.z)));
        }
        ctx.putImageData(imdata, 0, 0);
      };
    
      const binStlToTextStl = (stlBin) => {
        //console.log("Binary stl file...");
        const n =
          (stlBin.charCodeAt(80) + (stlBin.charCodeAt(81) << 8) +
            (stlBin.charCodeAt(82) << 16) + (stlBin.charCodeAt(83) << 24));
        //console.log('facets: ' + n + ' ::');
    
        let s = 'solid fakesolid\n';
        for (let p = 84; p < n * 50 + 84; p += 50) {
          const buffer = new ArrayBuffer(48);
          const byteView = new Uint8Array(buffer);
          for (let i = 0; i < 48; i++) {
            byteView[i] = stlBin.charCodeAt(p + i);
          }
          const floatView = new Float32Array(buffer);
          s = `${s}facet normal ${floatView[0]} ${floatView[1]} ${floatView[2]}\n`;
          s = `${s}outer loop\n`;
          s = `${s}vertex ${floatView[3]} ${floatView[4]} ${floatView[5]}\n`;
          s = `${s}vertex ${floatView[6]} ${floatView[7]} ${floatView[8]}\n`;
          s = `${s}vertex ${floatView[9]} ${floatView[10]} ${floatView[11]}\n`;
          s = `${s}endloop\n`;
          s = `${s}endfacet\n`;
        }
        s = `${s}endsolid fakesolid\n`;
        return s;
      };
    
      const textToFaces = (stlTxt) => {
        let ret = {
          min: { x: 10000, y: 10000, z: 10000 },
          max: { x: -10000, y: -10000, z: -10000 },
          faces: []
        };
        // lines: string[], i: number
        let onFacet = (lines, i) => {
          const face = { vertices: [], normal: { x: 0, y: 0, z: 0 } };
          let cmnds = lines[i].match(/[^ ]+/g);
          face.normal = {
            x: parseFloat(cmnds[2]),
            y: parseFloat(cmnds[2]),
            z: parseFloat(cmnds[2])
          };
          //let j = 0;
          while (true) {
            i++;
            cmnds = lines[i].match(/[^ ]+/g);
            if (cmnds[0] === 'endloop') {
              break;
            }
            if (cmnds[0] === 'vertex') {
              const v = {
                x: parseFloat(cmnds[1]),
                y: parseFloat(cmnds[2]),
                z: parseFloat(cmnds[3])
              };
              face.vertices.push(v);
              if (v.x < ret.min.x) {
                ret.min.x = v.x;
              }
              if (v.x > ret.max.x) {
                ret.max.x = v.x;
              }
              if (v.y < ret.min.y) {
                ret.min.y = v.y;
              }
              if (v.y > ret.max.y) {
                ret.max.y = v.y;
              }
              if (v.z < ret.min.z) {
                ret.min.z = v.z;
              }
              if (v.z > ret.max.z) {
                ret.max.z = v.z;
              }
              //j++;
            }
          }
          i++;
          return { face: face, i: i };
        };
        if (stlTxt[0] === 's') {
          const lines = stlTxt.match(/[^\r\n]+/g);
          let i = 1;
          while (i < lines.length) {
            const cmnds = lines[i].match(/[^ ]+/g);
            if (cmnds[0] === 'facet') {  // in facet
              const { face, i: ni } = onFacet(lines, i);
              i = ni;
              ret.faces.push(face);
            }
            i++;
          }
        } else {
        }
        return ret;
      };
    
      const drawStl = (stlData, canvas, dpi_, rotation_, margin) => {
        const byteCharacters = atob(stlData.substr(stlData.indexOf(',') + 1).replace(/\s/g, ''));

        let dpi = dpi_ ? dpi_ : 75;
        let rotation = rotation_ ? {
          x: rotation_.x * Math.PI / 180,
          y: rotation_.y * Math.PI / 180,
          z: rotation_.z * Math.PI / 180
        } : { x: 0, y: 0, z: 0 };
        let stl3D = {};
        if (byteCharacters[0] === 's') {
          stl3D = textToFaces(byteCharacters);
        } else {
          const txtStl =
            binStlToTextStl(byteCharacters);
          stl3D = textToFaces(txtStl);
        }
        stl3DDrawSimple(canvas, stl3D, dpi, rotation, margin);
    
      };
      let stl_to_canvas = {
        drawStl: drawStl,
        textToFaces: textToFaces,
        binStlToTextStl: binStlToTextStl,
        stl3DDrawSimple: stl3DDrawSimple,
        resetImage: resetImage
      };
    
      return stl_to_canvas;
};
  
  export default service_stl_converter;