/*

        Frontend for the pigcd
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import React, { useEffect, useRef } from 'react';

const CanvasImageContainer = ({ image, onImageUpdate }) => {
    const canvasRef = useRef();
  
    useEffect(() => {
      const updateCanvas = () => {
        const ctx = canvasRef.current.getContext('2d');
        const img = new Image();
  
        img.onload = () => {
          ctx.canvas.width = img.width;
          ctx.canvas.height = img.height;
          ctx.fillStyle = 'black';
          ctx.rect(0, 0, img.width, img.height);
          ctx.fill();
          ctx.drawImage(img, 0, 0, img.width, img.height);
          onImageUpdate(canvasRef.current.toDataURL());
        };
  
        img.src = image.src;
      };
  
      updateCanvas();
    }, [image, onImageUpdate]);
  
    return <canvas ref={canvasRef} />;
  };
  
  export default CanvasImageContainer;
  