# Service STL Converter

The `service_stl_converter` module is designed to provide functionalities to convert STL (Stereolithography) files to images, specifically PNG format.

## Available Functions

### `drawStl(byteCharacters, canvas, dpi, rotation, margin)`

This function converts STL data to an image and renders it on the provided canvas.

- `byteCharacters`: STL data in either text or binary format. This should be in base64 encoded form.
- `canvas`: The HTML canvas element where the image will be rendered.
- `dpi` (optional): Dots per inch, defaults to 75.
- `rotation` (optional): Object containing rotation values for x, y, and z axes in degrees.
- `margin` (optional): Margin value for the image.

### `textToFaces(stlTxt)`

Converts text-based STL data to an object representation containing vertices and face information.

- `stlTxt`: Text-based STL data.

### `binStlToTextStl(stlBin)`

Converts binary STL data to a human-readable text-based STL format.

- `stlBin`: Binary-based STL data.

### `stl3DDrawSimple(canvas, stl3D, dpi, rotation, margin)`

A helper function used internally to render the STL data on the canvas.

### `resetImage(canvas, size)`

Resets the canvas to its initial state with optional width and height values.

## Usage

```javascript
import { service_stl_converter } from 'service_stl_converter';

// Example usage
const stlService = service_stl_converter();

// Convert STL data to image
stlService.drawStl(stlData, canvasElement, dpiValue, rotationValues, marginValue);
