import React, { useState, useEffect } from 'react';
// import STLToPNGConverter from './components/STLToPNGConverter'; // Path to your STL to PNG converter component
import service_stl_converter from './components/stlConverter'; // Path to your STL to PNG conversion service

import TextField from "@mui/material/TextField";
import Slider from "@mui/material/Slider";
import Button from "@mui/material/Button";
import { Container, Stack } from '@mui/material';
import RotateLeftIcon from '@mui/icons-material/RotateLeft';
import RotateRightIcon from '@mui/icons-material/RotateRight';


const STLToolView = ({generatedImage, setGeneratedImage, stlFile, setStlFile, dpi}) => {
  const [rotationX, setRotationX] = useState(0);
  const [rotationY, setRotationY] = useState(0);
  const [rotationZ, setRotationZ] = useState(0);
  const [margin, setMargin] = useState(5);

  useEffect(function conversion(){
    // console.log('effect on ', stlFile);
    convertSTLToPNG();
  });

  const stlService = service_stl_converter();

  const handleFileChange = (event) => {

    const file = event.target.files[0];
    const reader = new FileReader();
    
    reader.onload = (event) => {
        setStlFile(event.target.result);
    };
    reader.readAsDataURL(file); // Or use `reader.readAsArrayBuffer(file)` for binary data
  };


  const convertSTLToPNG = () => {
    // Call the STL conversion service

    if (stlFile === null) {
      console.error('cannot render empty file', stlFile);
      return 0;
    }
    // Obtain the canvas element
    const canvas = document.createElement('canvas');//canvasRef.current;

    stlService.drawStl(stlFile, canvas, dpi, {x:rotationX, y:rotationY, z:rotationZ}, margin);

    // Get the image data from the canvas
    const image = canvas.toDataURL('image/png');

    // Invoke the callback with the generated image data
    setGeneratedImage(image);
  };


  return (
    <Container>

        <Stack spacing={2}>

        <Button variant="contained" component="label" >Select STL file
    <input type="file" accept=".stl" onChange={handleFileChange} hidden />
  </Button>

{Object.entries({
        rotationX: {step:10,inputProps:{min: -180,max: 180,inputMode: 'numeric',pattern: '[0-9.]*'}, value:rotationX, helperText: 'X Rotation', setter:setRotationX},
        rotationY: {step:10,inputProps:{min: -180,max: 180,inputMode: 'numeric',pattern: '[0-9.]*'}, value:rotationY, helperText: 'Y Rotation', setter:setRotationY},
        rotationZ: {step:10,inputProps:{min: -180,max: 180,inputMode: 'numeric',pattern: '[0-9.]*'}, value:rotationZ, helperText: 'Z Rotation', setter:setRotationZ},
        margin: {step:1,inputProps:{min: 0,max: 30,inputMode: 'numeric',pattern: '[0-9.]*'}, value:margin, helperText: 'Margin around object [mm]', setter:setMargin},
      }).map(([name, {step,inputProps,value,helperText, setter}]) => (
        <Stack spacing={2} direction="row" >
        <TextField
              key={name}
              onChange={event => setter(parseFloat(event.target.value))}
              inputProps={inputProps}
              name={name}
              label={name}
              value={value}
              helperText={helperText}
            />
          <RotateLeftIcon />
          <Slider aria-label="rotationX" value={value} step={step} min={inputProps.min} max={inputProps.max} onChange={event => setter(event.target.value)} />
          <RotateRightIcon /> 
        </Stack>
      ))}
  </Stack>    

    </Container>
  );
};

export default STLToolView;